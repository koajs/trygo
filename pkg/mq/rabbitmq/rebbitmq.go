package rabbitmq

import (
	"fmt"
	"github.com/streadway/amqp"
)

// GetRabbitmqConn
func GetRabbitmqConn() *amqp.Connection {

	// http://test.lab.gclove.com:15672/
	conn, err := amqp.Dial("amqp://mm:mm@test.lab.gclove.com:5672/mmhost")

	if err != nil {
		fmt.Println(err)
	}

	return conn
}