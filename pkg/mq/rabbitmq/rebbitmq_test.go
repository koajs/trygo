package rabbitmq

import (
	"fmt"
	"github.com/streadway/amqp"
	"testing"
)

// TestGetRabbitmqConn
func TestGetRabbitmqConn(t *testing.T) {
	GetRabbitmqConn()
}

// 生产者
func TestRabbitmqProducer(t *testing.T) {
	conn := GetRabbitmqConn()

	// 接下来让我们在 rabbitmq 连接上面打开一个 channel
	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}
	defer ch.Close()

	// ExchangeDeclare
	err = ch.ExchangeDeclare(
		"vl:queue_common", // name
		"direct",          // type
		false,             // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	if err != nil {
		fmt.Println(err)
	}

	// 在 channel 打开的清空下，我们可以声明 发布和订阅 的队列
	queue, err := ch.QueueDeclare(
		"vl:queue_common", // name
		true,              // durable
		false,             // delete when unused
		false,             // exclusive
		false,             // no-wait
		nil,               // arguments
	)
	if err != nil {
		fmt.Println(err)
	}

	// 我们可以在这里打印出队列的状态， 展示队列消息数量等信息
	fmt.Println("队列状态, 消费者", queue.Consumers, "未消费消息数", queue.Messages)

	err = ch.QueueBind(
		queue.Name,        // queue name
		"vl:queue_common", // routing key
		"vl:queue_common", // exchange
		false,
		nil,
	)

	if err != nil {
		fmt.Println(err)
	}

	// 发布 1 个消息到 rabbitmq

	err = ch.Publish(
		"vl:queue_common", // exchange, 这个随便写
		"vl:queue_common", // routing key
		false,             // mandatory
		false,             // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte("Hello julia!"),
		},
	)

	if err != nil {
		fmt.Println(err)
	}

}

// 测试消费者
func TestConsumer(t *testing.T) {
	conn := GetRabbitmqConn()

	ch, err := conn.Channel()
	if err != nil {
		fmt.Println(err)
	}
	defer ch.Close()

	if err != nil {
		fmt.Println(err)
	}

	msgs, err := ch.Consume(
		"vl:queue_common",
		"",
		false, // 自动确认消息
		false,
		false,
		false,
		nil,
	)

	forever := make(chan bool)

	go func() {
		for d := range msgs {
			fmt.Printf("消费者收到 vl:queue_common 消息: %s\n", d.Body)

			if d.Acknowledger == nil {
				fmt.Println("接收消息发生错误")
			} else {
				// multiple  true表示回复当前信道所有未回复的 ack，用于批量确认。false表示回复当前条目
				d.Acknowledger.Ack(d.DeliveryTag, false)
			}

			// 如果requeue为true，则RMQ会把这条消息重新加入队列，如果requeue为false，则RMQ会丢弃本条消息
			// d.Reject(true) // 拒绝并重新投递
		}
	}()

	fmt.Println(" [*] - 等待消息中 ...")

	<-forever
}
