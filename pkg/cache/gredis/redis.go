package gredis

import (
	"errors"
	"github.com/go-redis/redis/v8"
)

var (
	redisList map[string]*redis.Client
)


// InstDbList 初始化连接池
func InstRedisList() (err error){

	redisList = make(map[string]*redis.Client, 10)


	// 进行注册
	// 非 Cluster 连接
	redisOptions := &redis.Options{
		Addr:     "test.lab.gclove.com:23312",
		Password: "TKeBbvOQ4zEj",
		DB:       7,
	}
	redisList["redis_live"] = redis.NewClient(redisOptions)


	// 如果是 cluster 要用  redis.NewClusterClient
	// 返回值类型也是  *redis.ClusterClient

	return
}



// GetRedis 取得 redis 实例
// 参考文档 https://redis.uptrace.dev/
func GetRedis(redisName string) (*redis.Client, error) {

	if _, ok := redisList[redisName]; ok {
		return redisList[redisName], nil
	}

	return nil, errors.New("找不到 Redis 配置")
}