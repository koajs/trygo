package gredis

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"testing"
)

// TestRedisSetAndGet 测试 redis set 和 get 命令
func TestRedisSetAndGet(t *testing.T) {

	redisClient, _ := GetRedis("redis_live")

	var ctx = context.Background()


	// 执行 Redis Set 命令
	err := redisClient.Set(ctx, "username", "李白哦", 0).Err()
	if err != nil {
		fmt.Println("redis set fail", err)
		panic(err)
	}


	// 执行 Redis get 命令, 取不存在的值
	val, err := redisClient.Get(ctx, "username_30000520").Result()

	if err == redis.Nil {
		fmt.Println("username_30000520 不存在")
	} else if err != nil {
		panic(err) // 可能是出错了
	} else {
		fmt.Println("key username_30000520 是:", val)
	}


	// 执行 Redis get 命令, 取存在的值
	val, err = redisClient.Get(ctx, "username").Result()

	if err == redis.Nil {
		fmt.Println("username 不存在")
	} else if err != nil {
		panic(err) // 可能是出错了
	} else {
		fmt.Println("key username 是:", val)
	}

}


// TestRedisHashmap 测试 redis hash 相关 命令
func TestRedisHashmap(t *testing.T) {

	redisClient, _ := GetRedis("redis_live")

	fmt.Println(redisClient)

	// var ctx = context.Background()


	// https://github.com/go-redis/redis/blob/master/example_test.go

	// 自行尝试


	// - HSet("myhash", "key1", "value1", "key2", "value2")
	// - HSet("myhash", []string{"key1", "value1", "key2", "value2"})
	// - HSet("myhash", map[string]interface{}{"key1": "value1", "key2": "value2"})

}
