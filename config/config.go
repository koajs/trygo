package config

import (
	"gitee.com/koajs/trygo/routers"
	"github.com/gin-gonic/gin"
)

// App 结构体
// 结构体对应 PHP 里的  Class
type App struct {
	AppName string // app 的名字
	AppEnv string // 运行环境, 可选  prod/dev
	RouteEngine *gin.Engine // gin 路由引擎实例
}

var (
	// 定义全局的 App 对象变量
	AppInstant App
)

// 给 App 结构体添加一个 Run 方法，用来启动 HTTP 服务
func (app App) Run() {


	// 创建一个 gin 框架的默认实例
	app.RouteEngine = gin.Default()

	// 通过独立的函数注册路由，因为我们传入的参数是个指针
	routers.RegRouters(app.RouteEngine)

	// 开始监听 0.0.0.0:8080
	app.RouteEngine.Run(":8080")
}
