package db

import (
	"encoding/json"
	"fmt"
	"gitee.com/koajs/trygo/routers/v2/user/models"
	"testing"
)

func TestGetDb(t *testing.T) {
	db, err := GetDb("users")

	if err != nil {
		fmt.Println("db 连接失败", err)
	}
	fmt.Println(db)
	fmt.Println(db.Config)
}

func TestSelectRows(t *testing.T) {
	db, err := GetDb("users")

	if err != nil {
		fmt.Println("db 连接失败", err)
	}

	var users []models.User

	db.Where("auto_id > ?", 0).Find(&users)

	fmt.Println(users)

	fmt.Printf("%#v \n", users)

	byte1, _ := json.Marshal(users)

	fmt.Println(string(byte1))

}
