module gitee.com/koajs/trygo

go 1.14

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/go-redis/redis/v8 v8.7.1
	github.com/streadway/amqp v1.0.0
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.21.2
)
