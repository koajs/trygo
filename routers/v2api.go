package routers

import (
	v2 "gitee.com/koajs/trygo/routers/v2" // 在 import 前缀前面可以写个别名
	"gitee.com/koajs/trygo/routers/v2/user/auth"
	"github.com/gin-gonic/gin"
)

// RegRouters 注册路由的方法
func RegRouters(router *gin.Engine)  {

	// 注册路由，对应 http://localhost:8080/ping
	router.GET("/ping", v2.PingHandler)

	// TODO: 在这里注册路由

	// 比如我们要注册 {{api_url}}/voiceapp/login/sms 这个路由

	// 可能会需要一个路由组，因为所有路由都有 voiceapp

	voiceappRouteGroup := router.Group("/voiceapp")
	// 也可以把 voiceappRouteGroup 传给一个独立的函数执行注册任务
	{
		// 登录路由
		voiceappRouteGroup.Any("/login/sms", auth.LoginBySmsHandler)
	}


	
}