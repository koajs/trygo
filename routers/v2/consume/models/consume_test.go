package models

import (
	"encoding/json"
	"fmt"
	db2 "gitee.com/koajs/trygo/db"
	"testing"
)

func TestTryConsume(t *testing.T) {

	db, err := db2.GetDb("billing")

	if err != nil {
		fmt.Println("db 连接失败", err)
	}
	fmt.Println(db)

	// tb_consume_202103


	var consumeSlice []TbConsume

	// https://gorm.io/zh_CN/docs/conventions.html


	db.Table("tb_consume_202103").Where("auto_id > 0").Find(&consumeSlice)

	fmt.Println(consumeSlice)

	byte1, _ := json.Marshal(consumeSlice)

	fmt.Println(string(byte1))



}