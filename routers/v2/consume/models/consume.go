package models

import "time"

// TbConsume tb_consume_202103 表, 消费表（月建）-by yuliang
type TbConsume struct {
	AutoID             uint      `gorm:"column:auto_id" json:"auto_id"`
	Sn                 string   `gorm:"column:sn" json:"sn"`
	// Sn                 float64   `gorm:"column:sn" json:"sn"`
	Pfid               int32     `gorm:"column:pfid" json:"pfid"`
	OpType             int32     `gorm:"column:op_type" json:"op_type"`
	ProdID             int32     `gorm:"column:prod_id" json:"prod_id"`
	ProdPrice          int32     `gorm:"column:prod_price" json:"prod_price"`
	ProdCnt            int32     `gorm:"column:prod_cnt" json:"prod_cnt"`
	ProdTotal          int32     `gorm:"column:prod_total" json:"prod_total"`
	ReceiveProdTotal   int32     `gorm:"column:receive_prod_total" json:"receive_prod_total"`
	ReceiveAvatarTotal int32     `gorm:"column:receive_avatar_total" json:"receive_avatar_total"`
	TPfid              int32     `gorm:"column:t_pfid" json:"t_pfid"`
	LiveID             string    `gorm:"column:live_id" json:"live_id"`
	ActID              int32     `gorm:"column:act_id" json:"act_id"`
	IsBag              int8      `gorm:"column:is_bag" json:"is_bag"`
	ConsumeTime        time.Time `gorm:"column:consume_time" json:"consume_time"`
	ParentSn           float64   `gorm:"column:parent_sn" json:"parent_sn"`
	Notes              string    `gorm:"column:notes" json:"notes"`
	Ext1               string    `gorm:"column:ext1" json:"ext1"`
	Ext2               string    `gorm:"column:ext2" json:"ext2"`
	Ext3               string    `gorm:"column:ext3" json:"ext3"`
	// 一般需要给 time.Time 设定一个别名
	DbInsertTime       time.Time `gorm:"column:db_insert_time" json:"db_insert_time"`
	DbUpdateTime       time.Time `gorm:"column:db_update_time" json:"db_update_time"`
}


// TableName 会将 User 的表名重写为 `tb_users`
func (TbConsume) TableName() string {

	// TODO: 处理分表
	//  https://gorm.io/zh_CN/docs/conventions.html


	return "tb_consume_202102"
}

