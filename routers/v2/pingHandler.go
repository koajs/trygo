package v2

import "github.com/gin-gonic/gin"


// PingHandler /ping 路由的处理方法
func PingHandler(context *gin.Context) {

	// TODO: 可以在这里执行一些逻辑

	// gin.H is a shortcut for map[string]interface{}

	// 通过 context.JSON 返回 json 对象
	context.JSON(
		200,
		gin.H{
			"ret_code": 0,
			"ret_msg": "pong",
			"data": gin.H{},
		},
	)
}