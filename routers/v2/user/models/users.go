package models

import (
	"time"
)

// 用户模型，对应用户表里的一条记录
type User struct {
	AutoID       uint      `gorm:"column:auto_id;primarykey"`
	Pfid         uint      `gorm:"column:pfid"` // 用户的 PFID
	Mobile       string    `gorm:"column:mobile" json:"mobile"`
	Nickname     string    `gorm:"column:nickname" json:"user_nickname"`
	Password     string    `gorm:"column:password"`
	Headimg      string    `gorm:"column:headimg"`
	Token        string    `gorm:"column:token"`
	DbInsertTime time.Time `gorm:"column:db_insert_time"`
	DbUpdateTime time.Time `gorm:"column:db_update_time"`
}

// TableName 会将 User 的表名重写为 `tb_users`
func (User) TableName() string {
	return "tb_users"
}
