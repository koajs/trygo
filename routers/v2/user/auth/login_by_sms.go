package auth

import (
	"fmt"
	"gitee.com/koajs/trygo/db"
	"gitee.com/koajs/trygo/routers/v2/user/models"
	"github.com/gin-gonic/gin"
)

// LoginBySmsHandler 登录逻辑
func LoginBySmsHandler(context *gin.Context) {

	// TODO: 可以在这里执行一些逻辑

	// 1. 获取 post 表单数据
	cell := context.PostForm("cell")
	code := context.DefaultPostForm("code", "默认值")

	fmt.Println(cell, code)

	// TODO:  2. 查询数据库，并生成 token

	db, err := db.GetDb("users")
	if err != nil {
		fmt.Println("db 连接失败", err)
	}

	// 2.1 查找有没有这个用户
	var user models.User
	ret := db.Where("mobile = ?", cell).First(&user)

	var users []models.User
	ret = db.Where("mobile = ?", cell).Find(&users)

	// 查找的结果
	fmt.Println(ret.Error, user)

	// 2.2 更新字段
	user.Token = "新 Token"
	db.Save(&user)

	// TODO:  3. 返回结果
	// gin.H 是 map[string]interface{} 类型的别名

	// 通过 context.JSON 返回 json 对象
	context.JSON(
		200,
		gin.H{
			"ret_code": 0,
			"ret_msg":  "登录成功",
			"data": gin.H{
				"one_user":  user,
				"one_user2": users,
			},
		},
	)
}
