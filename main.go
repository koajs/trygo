package main

import (
	"gitee.com/koajs/trygo/config"
	"gitee.com/koajs/trygo/db"
	"gitee.com/koajs/trygo/pkg/cache/gredis"
)

// main
// 和 熟知的 PHP/Python 等 脚本语言按行执行  不同
// main 包下面的 main 函数是程序执行的入口
func main() {

	var err error

	// TODO: 第一步应该是加载配置文件

	// TODO 第二步应该是创建日志文件

	// TODO 第三步可以是测试 数据库，队列 等服务是否异常

	// 初始化 mysql 连接池
	err = db.InstDbList()
	if err != nil {
		panic(err)
	}

	// 初始化 redis 连接池
	err = gredis.InstRedisList()
	if err != nil {
		panic(err)
	}

	// 如果前面的步骤无法完成，就需要 Panic 退出程序
	// panic("无法继续执行程序")


	// 关于 config.AppInstant

	// config 是我们导入的包名
	// AppInstant 是一个 config 包下的 AppInstant 全局变量

	// TODO:: 相当于 php 里的   config::AppInstant

	// 并且它是结构体类型，已经赋值并创建内存空间

	// TODO: 而 config.AppInstant.Run 就相当于  (config::AppInstant)->Run();

	// 我们调用 AppInstant 这个 结构体 的 Run 方法，注册路由并开始监听 HTTP 端口
	config.AppInstant.Run()

}



